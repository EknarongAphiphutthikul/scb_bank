import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  constructor(private router: Router, @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

  ngOnInit() {
  }

  register() {
    this.router.navigate(['/register']);
  }

  unregister() {
    this.router.navigate(['/unregister']);
  }

  home() {
    this.router.navigate(['/home']);
  }

  logout(){
    window.localStorage.clear()
    this.router.navigate(['/']);
  }
}
