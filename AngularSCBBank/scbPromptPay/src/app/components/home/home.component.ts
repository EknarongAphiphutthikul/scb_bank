import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { CalInterbankService } from '../../service/calinterbank.service';
import { HttpErrorResponse } from "@angular/common/http";
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { Response } from '@angular/http'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  config: SwiperOptions = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 30
  };
  id: any
  account: any
  act: any
  acountIdStatement: any
  acountStatement=false

  constructor(private router: Router,private call_interbankservice: CalInterbankService,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

  ngOnInit() {
    this.id = this.storage.get("id");
    if(this.id == undefined){
      this.router.navigate(['/'])
    }
    console.log(this.id)
    this.callGetService();
  }

  data={

  }

  setAccount(account: any) {
    this.account = account
    this.storage.set("account", account)
    console.log(account)
  }

  callGetService() {
    this.call_interbankservice.home(this.id).then(response => {
      let account = response.json();
      console.log(account);
      this.setAccount(account)
      // console.log('Test' + res[0].name);
    }).catch((err: HttpErrorResponse) => {
      if (err.status == 400) {
        console.log("Don't have id. Bad request")
      } else if (err.status == 404) {
        let mgs = JSON.parse(err["_body"]);
        console.log(mgs.err)
      } else {
        console.log("Internal server error")
      }
    });
  }

  cardInfo(accounId) {
    this.acountStatement = true
    console.log(accounId)
    this.data = {
      "accountId": accounId
    }
    this.callStatement()
  }

  callStatement() {
    this.call_interbankservice.stateFive(this.data).then(
        (response: Response) => {
            let data = response.json();
            console.log(data[0]);
            this.setAcountStatement(data)
            if(data[0] == undefined){
              alert("Data not fount!")
            }
        })
        .catch((err: HttpErrorResponse) => {
            let mgs = JSON.parse(err["_body"]);
            console.log(mgs.err)
            alert("Data not fount!")
        });
  }

  setAcountStatement(acountIdStatement){
    this.acountIdStatement = acountIdStatement
  }
  
}