import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { CalInterbankService } from '../../service/calinterbank.service';
import { Response } from '@angular/http'
import { HttpErrorResponse } from "@angular/common/http";
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = '';
  pwd = '';
  detail = {
  }
  id: any

  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private router: Router, private call_interbankservice: CalInterbankService) { }

  ngOnInit() {
  }

  getDetail() {
    this.detail = {
      "login": this.user,
      "password": this.pwd
    }
    return this.detail;
  }

  login() {
    this.callPostService();
  }

  sendId(id: any) {
    this.storage.set("id", id);
    return this.router.navigate(['/home']);
  }

  callPostService() {
    console.log("Bussiness List");
    this.call_interbankservice.login(this.getDetail()).then(
      (response: Response) => {
        let data = response.json();
        this.sendId(data.id).then(
          (rest: any) => {
            rest;
          })
      }).catch((err: HttpErrorResponse) => {
        if (err.status == 404) {
          let mgs = JSON.parse(err["_body"]);
          alert(mgs.err);
        } else {
          console.log("Internal server error.")
        }
      });
  }
}
