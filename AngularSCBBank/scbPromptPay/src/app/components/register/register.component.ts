import { Component, OnInit, Inject } from '@angular/core';
import { CalInterbankService } from '../../service/calinterbank.service';
import { Response } from '@angular/http'
import { Router } from '@angular/router';
import { HttpErrorResponse } from "@angular/common/http";
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    constructor(private router: Router, private call_interbankservice: CalInterbankService,
        @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

    ngOnInit() {
        this.id = this.storage.get("id");
        if (this.id == undefined) {
            this.router.navigate(['/'])
        }
        this.account = this.storage.get("account")
        this.selectedAccount = this.account[0].accountId
        console.log(this.account)
    }
    
    id:any
    account: any;
    detail = {
    }
    type = true;
    citizen = "";
    mobile = "";
    mgs = {
        status: false,
        err: ""
    }
    selectedAccount: any

    getDetail() {
        let type_value;
        if (this.type) {
            type_value = "mobile";
            this.detail = {
                "IDType": type_value,
                "IDValue": this.mobile,
                "BankCode": this.account[0].bankCode,
                "AccountID": this.selectedAccount,
                "AccountName": this.account[0].name
            }
        } else {
            type_value = "citizen";
            this.detail = {
                "IDType": type_value,
                "IDValue": this.citizen,
                "BankCode": this.account[0].bankCode,
                "AccountID": this.selectedAccount,
                "AccountName": this.account[0].name
            }
        }
        return this.detail;
    }

    validate() {
        if (this.type) {
            if (this.mobile.length == 10) {
                this.mgs.status = true;
            }
            else
                this.mgs.status = false;
            this.mgs.err = "Please enter your Mobile number";
        } else {
            console.log(this.citizen)
            if (this.citizen.length == 13) {
                this.mgs.status = true;
            }
            else
                this.mgs.status = false;
            this.mgs.err = "Please enter your ID Card";
        }
    }

    register() {
        this.validate();
        if (this.mgs.status) {
            this.mgs.err = "";
            this.callPostService();
        }
    }

    callPostService() {
        console.log(this.getDetail());
        this.call_interbankservice.register(this.getDetail()).then(
            (response: Response) => {
                let data = response.json();
                console.log(this.detail);
                console.log(data);
                alert(data.status);
            })
            .catch((err: HttpErrorResponse) => {
                let mgs = JSON.parse(err["_body"]);
                console.log(mgs.err.split(" ")[0]);
                if (mgs.err.split(" ")[0] == 400) {
                    alert("Duplicate account!")
                } else {
                    alert("Internal server errer!")
                }
            });
    }

    getAccountFromUI(accountValue) {
        this.selectedAccount = accountValue;
        console.log(this.selectedAccount)
    }
}