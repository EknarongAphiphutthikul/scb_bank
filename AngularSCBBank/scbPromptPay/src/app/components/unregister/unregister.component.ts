import { Component, OnInit, Inject } from '@angular/core';
import { CalInterbankService } from '../../service/calinterbank.service';
import { HttpErrorResponse } from "@angular/common/http";
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unregister',
  templateUrl: './unregister.component.html',
  styleUrls: ['./unregister.component.css']
})
export class UnregisterComponent implements OnInit {

  constructor(private router: Router,private call_interbankservice: CalInterbankService,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }
  type = true;
  citizen = "";
  mobile = "";
  mgs = {
    status: false,
    err: ""
  }
  mb = 'mobile'
  cz ='citizen'
  id:any 

  ngOnInit() {
    this.id = this.storage.get("id");
    if(this.id == undefined){
      this.router.navigate(['/'])
    }
  }

  validate() {
    if (this.type) {
      if (this.mobile.length == 10) {
        this.mgs.status = true;
      }
      else
        this.mgs.status = false;
      this.mgs.err = "Please enter your Mobile number";
    } else {
      if (this.citizen.length == 13) {
        this.mgs.status = true;
      }
      else
        this.mgs.status = false;
      this.mgs.err = "Please enter your ID Card";
    }
  }

  unregister() {
    this.validate()
    if (this.mgs.status) {
      this.mgs.err = "";
      this.sendValue();
    }
  }

  sendValue() {
    if (this.type){
      this.storage.set("type",this.mb)
    } else {
      this.storage.set("type",this.cz)
    }

    this.storage.set("idcard",this.citizen)
    this.storage.set("mobile",this.mobile)
    this.callDelteService()
  }

  callDelteService() {
    this.call_interbankservice.unregis().then(response => {
      let mgs = response.json()
      alert(mgs.status)
    }).catch((err: HttpErrorResponse) => {
      let mgs = JSON.parse(err["_body"]);
      console.log(mgs.err);
      alert(mgs.err)
    })
  }
}
