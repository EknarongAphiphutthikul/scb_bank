import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfermPromptPayComponent } from './conferm-prompt-pay.component';

describe('ConfermPromptPayComponent', () => {
  let component: ConfermPromptPayComponent;
  let fixture: ComponentFixture<ConfermPromptPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfermPromptPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfermPromptPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
