import { Component, OnInit, Inject } from '@angular/core';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { CalInterbankService } from '../../service/calinterbank.service';
import { Response } from '@angular/http'
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-conferm-prompt-pay',
  templateUrl: './conferm-prompt-pay.component.html',
  styleUrls: ['./conferm-prompt-pay.component.css']
})
export class ConfermPromptPayComponent implements OnInit {

  constructor(private router: Router,@Inject(LOCAL_STORAGE) private storage: WebStorageService,
  private call_interbankservice: CalInterbankService) { }

  detail = {

  }
  account: any
  type = true
  selectedAccount: any
  citizen: any
  mobile: any
  value: any
  amount: any
  accountname:any
  accountId:any 

  getDetail() {
    this.detail = {
      "sendAccountId": this.storage.get("select"),
      "sendBankCode": this.account[0].bankCode,
      "type": this.storage.get("value"),
      "value": this.storage.get("valueAct"),
      "amountBalance": this.storage.get("amount")
    }
    return this.detail
  }

  ngOnInit() {
    this.accountname = this.storage.get("sendAct").AccountName
    this.accountId = this.storage.get("sendAct").AccountID
    this.account = this.storage.get("account")

  }

  confirm(){
    console.log(this.getDetail());
    this.callPostService()
  }

  callPostService() {
    console.log(this.getDetail());
    this.call_interbankservice.promptpay(this.getDetail()).then(
      (response: Response) => {
        let data = response.json();
        console.log(this.detail);
        console.log(data);
        alert(data.result)
        this.router.navigate(['/promptpay'])
      })
      .catch((err: HttpErrorResponse) => {
        let mgs = JSON.parse(err["_body"]);
         if (err.status == 404) {
          console.log(mgs.err)
          alert("Account not found!")
        // } else if (mgs.message == '424 null') {
        //   alert("Complete transfer by promptpay")
        //   this.router.navigate(['/promptpay'])
        } else if (err.status == 400){
          alert("Balance amount incorrect!")
        }
      });
  }
}
