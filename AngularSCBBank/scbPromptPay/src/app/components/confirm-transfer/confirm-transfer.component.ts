import { Component, OnInit, Inject } from '@angular/core';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { CalInterbankService } from '../../service/calinterbank.service';
import { Response } from '@angular/http'
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-transfer',
  templateUrl: './confirm-transfer.component.html',
  styleUrls: ['./confirm-transfer.component.css']
})
export class ConfirmTransferComponent implements OnInit {

  constructor( @Inject(LOCAL_STORAGE) private storage: WebStorageService,
  private call_interbankservice: CalInterbankService, private router: Router) { }
  transferInfo = {

  }
  amount: any
  receive:any
  selectedAccount:any

  getTransferInfo() {
    this.transferInfo = {
      "accountIdSend":this.storage.get("select"),
      "accountIdReceive": this.storage.get("receive"),
      "balanceAmount": this.storage.get("amount")
    }
    return this.transferInfo
  }

  ngOnInit() {
  }

  confirm(){
    this.callPostServiceTransfer()
  }
  
  callPostServiceTransfer() {
    this.call_interbankservice.transferWithAmount(this.getTransferInfo()).then(
      (response: Response) => {
        let trans = response.json()
        console.log(trans)
        alert(trans.mgs)
        this.router.navigate(['/transfer'])
      }).catch((err: HttpErrorResponse) => {
        let mgs = JSON.parse(err["_body"]);
        console.log(mgs.err)
        if (err.status == 403) {
          alert(mgs.err)
        }
      })
  }

}
