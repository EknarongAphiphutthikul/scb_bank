import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { CalInterbankService } from '../../service/calinterbank.service';
import { Response } from '@angular/http'
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: 'app-promptpay',
  templateUrl: './promptpay.component.html',
  styleUrls: ['./promptpay.component.css']
})
export class PromptpayComponent implements OnInit {

  constructor(private router: Router, @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private call_interbankservice: CalInterbankService) { }

  account: any
  type = true
  selectedAccount: any
  citizen: any
  mobile: any
  value: any
  amount: any
  valueAct: any
  id:any

  ngOnInit() {
    this.id = this.storage.get("id");
    if(this.id == undefined){
      this.router.navigate(['/'])
    }
    this.account = this.storage.get("account")
    this.selectedAccount = this.account[0].accountId
  }

  promptPay() {
    this.storage.set("select", this.selectedAccount)
    this.storage.set("amount", this.amount)
    if (this.type) {
      this.value = 'mobile'
      this.valueAct = this.mobile
      this.storage.set("value", this.value)
      this.storage.set("valueAct", this.mobile)
    } else {
      this.value = 'citizen'
      this.valueAct = this.citizen
      this.storage.set("value", this.value)
      this.storage.set("valueAct", this.citizen)
    }
    this.callGetService()
  }

  getAccountFromUI(accountValue) {
    this.selectedAccount = accountValue;
    console.log(this.selectedAccount)
  }

  callGetService() {
    this.call_interbankservice.getaipid(this.value, this.valueAct).then(response => {
      let account = response.json()
      console.log(account)
      this.setAccountReceive(account).then((res) => {
        res;
      })
    }).catch((err: HttpErrorResponse) => {
      let mgs = JSON.parse(err["_body"]);
      alert(mgs.err)
    });
  }

  setAccountReceive(account: any) {
    this.storage.set("sendAct", account)
    return this.router.navigate(['/comfirmpromptpay'])
  }
}
