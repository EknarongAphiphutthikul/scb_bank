import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { CalInterbankService } from '../../service/calinterbank.service';
import { Response } from '@angular/http'
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  constructor(private router: Router, @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    private call_interbankservice: CalInterbankService) { }

  account: any
  selectedAccount: any
  detail = {

  }
  receive: any
  amount: any
  id:any 
  
  ngOnInit() {
    this.id = this.storage.get("id");
    if(this.id == undefined){
      this.router.navigate(['/'])
    }
    this.account = this.storage.get("account")
    this.selectedAccount =  this.account[0].accountId
  }

  getDetail() {
    this.detail = {
      "accountIdReceive": this.receive
    }
    return this.detail
  }

  getAccountFromUI(accountValue) {
    this.selectedAccount = accountValue;
    console.log(this.selectedAccount)
  }

  transfer() {
    console.log(this.account[0].accountId + " " + this.receive + " " + this.amount)
    this.storage.set("select", this.selectedAccount)
    this.storage.set("receive",this.receive)
    this.storage.set("amount",this.amount)
    this.callPostService();
  }

  callPostService() {
    this.call_interbankservice.transfer(this.getDetail()).then(
      (response: Response) => {
        let data = response.json()
        console.log(data[0].name)
        this.storage.set("nameReceive", data[0].name)
        console.log(this.storage.get("statusRes"))
        this.router.navigate(['/comfirmtrans'])
      }).catch((err: HttpErrorResponse) => {
        let mgs = JSON.parse(err["_body"]);
        if (err.status == 404) {
          alert(mgs.err)
        }
      })
  }

}
