import { Injectable, OnInit, Inject } from "@angular/core";
import { Headers, Http, RequestOptions } from "@angular/http";
import { WebStorageService, LOCAL_STORAGE } from "angular-webstorage-service";

@Injectable()
export class CalInterbankService {

    constructor(private http: Http, 
        @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

    uri:any;

    login(login: any) {
        return this.http
            .post('http://localhost:8080/scb_bank/checkLogin', login, ).toPromise();
    }

    register(reg: any) {
        return this.http
            .post('http://localhost:8080/scbbank/register', reg, ).toPromise();
    }

    home(idUser): any{
        return this.http.get('http://localhost:8080/scb_bank/showAccount/'+idUser).toPromise();
    }

    getaipid(type, value): any{
        return this.http.get('http://localhost:8080/scbbank/getPromptPay/'+type+'/'+value).toPromise();
    }

    unregis(){
        let type = this.storage.get("type")
        let number = this.storage.get("mobile")
        let idcard = this.storage.get("idcard")
        
        if(type == 'mobile'){
            this.uri = 'http://localhost:8080/scbbank/delete/'+type+'/'+number
        } else {
            this.uri = 'http://localhost:8080/scbbank/delete/'+type+'/'+idcard
        }

        return this.http
        .delete(this.uri).toPromise();
    }

    transfer(accountReceive){
        return this.http
        .post('http://localhost:8080/scb_bank/getReceiveAccount', accountReceive).toPromise();
    }

    transferWithAmount(transferInfo){
        return this.http
        .post('http://localhost:8080/scb_bank/transfer', transferInfo).toPromise();
    }

    promptpay(promptpay){
        return this.http
        .post('http://localhost:8080/scbbank/promptPay', promptpay).toPromise();
    }
    
    stateFive(statement){
        return this.http
        .post('http://localhost:8080/scb_bank/fiveStatement', statement).toPromise();
    }
}