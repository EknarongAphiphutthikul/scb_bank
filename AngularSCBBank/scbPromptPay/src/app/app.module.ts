import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HeaderbarComponent } from './components/headerbar/headerbar.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'angular2-useful-swiper';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { CalInterbankService } from './service/calinterbank.service';
import { ButtonComponent } from './components/button/button.component';
import { TransferComponent } from './components/transfer/transfer.component';
import { MatCardModule } from '@angular/material/card';
import { PromptpayComponent } from './components/promptpay/promptpay.component';
import { UnregisterComponent } from './components/unregister/unregister.component';
import { NumberOnlyDirective } from './service/numberformat.directive';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { StorageServiceModule} from 'angular-webstorage-service';
import { ConfirmTransferComponent } from './components/confirm-transfer/confirm-transfer.component';
import { ConfermPromptPayComponent } from './components/conferm-prompt-pay/conferm-prompt-pay.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'transfer', component: TransferComponent },
  { path: 'promptpay', component: PromptpayComponent },
  { path: 'unregister', component: UnregisterComponent },
  { path: 'comfirmtrans', component: ConfirmTransferComponent },
  { path: 'comfirmpromptpay', component: ConfermPromptPayComponent },
  { path: '**', component: PagenotfoundComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    HeaderbarComponent,
    TransferComponent,
    ButtonComponent,
    PromptpayComponent,
    UnregisterComponent,
    NumberOnlyDirective,
    PagenotfoundComponent,
    ConfirmTransferComponent,
    ConfermPromptPayComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    SwiperModule,
    HttpModule,
    MatCardModule,
    StorageServiceModule
  ],
  providers: [CalInterbankService],
  bootstrap: [AppComponent]
})
export class AppModule { }
