package com.orcsoft.training.tulaya.controller;

@SuppressWarnings("serial")
public class ExceptionInternalError extends Exception {
	public ExceptionInternalError(String msg) {
		super(msg);
	}
}