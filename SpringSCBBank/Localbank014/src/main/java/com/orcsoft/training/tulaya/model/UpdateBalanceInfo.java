package com.orcsoft.training.tulaya.model;

public class UpdateBalanceInfo {

	private String accountIdSend;
	private String accountIdReceive;
	private double balanceAmount;

	public String getAccountIdSend() {
		return accountIdSend;
	}

	public void setAccountIdSend(String accountIdSend) {
		this.accountIdSend = accountIdSend;
	}

	public String getAccountIdReceive() {
		return accountIdReceive;
	}

	public void setAccountIdReceive(String accountIdReceive) {
		this.accountIdReceive = accountIdReceive;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

}
