package com.orcsoft.training.tulaya.connectDB;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.orcsoft.training.tulaya.model.Statement;
import com.orcsoft.training.tulaya.model.TransferStatement;
import com.orcsoft.training.tulaya.model.TransferTxnInfo;

public class TransferTxnDAO {

	public int updateTxn(TransferTxnInfo act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper txnMapper = session.getMapper(TransferTxnMapper.class);
		int res = txnMapper.updateTxn(act);
		if (res != 1) {
			session.rollback();
		} else {
			session.commit();
		}
		session.close();
		return res;
	}

	public int selectTxn(TransferTxnInfo act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper txnMapper = session.getMapper(TransferTxnMapper.class);
		int res = txnMapper.selectTxn(act);
		session.close();
		return res;
	}

	public int updateState(TransferTxnInfo req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper txnMapper = session.getMapper(TransferTxnMapper.class);
		int res = txnMapper.updateState(req);
		session.commit();
		session.close();
		return res;
	}

	public List<TransferStatement> staementFive(Statement act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper txnMapper = session.getMapper(TransferTxnMapper.class);
		List<TransferStatement> res = txnMapper.staementFive(act);
		session.close();
		return res;
	}

	public List<TransferStatement> staementDuration(Statement act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper txnMapper = session.getMapper(TransferTxnMapper.class);
		List<TransferStatement> res = txnMapper.staementDuration(act);
		session.close();
		return res;
	}
}
