package com.orcsoft.training.tulaya.connectDB;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.orcsoft.training.tulaya.model.AccountInfo;
import com.orcsoft.training.tulaya.model.ReceivePromptPayInfo;
import com.orcsoft.training.tulaya.model.ShowAccountInfo;

public class ShowAccountDAO {

	public List<ShowAccountInfo> getAccount(int customerID) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		ShowAccountMapper actMapper = session.getMapper(ShowAccountMapper.class);
		List<ShowAccountInfo> act = actMapper.getAccount(customerID);
		session.close();
		return act;
	}
	
	public List<ShowAccountInfo> getAccountReceive(AccountInfo account) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		ShowAccountMapper actMapper = session.getMapper(ShowAccountMapper.class);
		List<ShowAccountInfo> act = actMapper.getAccountReceive(account);
		session.close();
		return act;
	}
	
	public List<ShowAccountInfo> checkAccoucnt(ReceivePromptPayInfo act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		ShowAccountMapper actMapper = session.getMapper(ShowAccountMapper.class);
		List<ShowAccountInfo> acccount = actMapper.checkAccoucnt(act);
		session.close();
		return acccount;
	}
}
