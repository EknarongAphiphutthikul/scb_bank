package com.orcsoft.training.tulaya.controller;

import java.sql.SQLException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orcsoft.training.tulaya.connectDB.AccountDAO;
import com.orcsoft.training.tulaya.connectDB.TransferTxnDAO;
import com.orcsoft.training.tulaya.model.GetPromptPayInfo;
import com.orcsoft.training.tulaya.model.RegisterInfo;
import com.orcsoft.training.tulaya.model.ResultPromptPayInfo;
import com.orcsoft.training.tulaya.model.TransferTxnInfo;
import com.orcsoft.training.tulaya.model.UserPromptPayInfo;
import com.orcsoft.training.tulaya.model.promptPayInfo;
import com.orcsoft.training.tulaya.service.CallRestService;
import com.orcsoft.training.tulaya.service.CallRestService.GetStatusCode;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/scbbank")
public class CallRestApiController {

	@Autowired
	CallRestService callRestService;

	@PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GetStatusCode> register(HttpServletRequest request, @RequestBody RegisterInfo req)
			throws ExceptionInternalError {
		return callRestService.registerInfo(req);
	}

	@GetMapping(value = "/getPromptPay/{type}/{value}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public GetPromptPayInfo getPromptPay(HttpServletRequest request, @PathVariable("type") String type,
			@PathVariable("value") String value) throws ExceptionNotFound {
		return callRestService.getPromptPay(type, value);
	}

	@DeleteMapping(value = "/delete/{type}/{value}")
	public ResponseEntity<GetStatusCode> deletePromptPay(HttpServletRequest request, @PathVariable("type") String type,
			@PathVariable("value") String value) throws ExceptionInternalError, ExceptionNotFound {
		int apid = callRestService.getPromptPay(type, value).getAIPID();
		return callRestService.deletePromptPay(apid);
	}

	@PostMapping(value = "/promptPay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResultPromptPayInfo> transferPromptPay(HttpServletRequest request,
			@RequestBody UserPromptPayInfo ptp)
			throws ExceptionDatabaseDown, ExceptionBalanceAmount, ExceptionNotFound {
		GetPromptPayInfo receiveFromPromptPay = getPromptPay(request, ptp.getType(), ptp.getValue());
		try {
			TransferTxnInfo txnInsert = new TransferTxnInfo();
			TransferTxnDAO transfer = new TransferTxnDAO();
			AccountDAO account = new AccountDAO();
			Date date = new Date();
			if (account.getBalanceAmountPP(ptp) < ptp.getAmountBalance() || ptp.getAmountBalance() == 0) {
				throw new ExceptionBalanceAmount("Balance amount incorrect!");
			} else if (account.getBalanceAmountPP(ptp) >= ptp.getAmountBalance() && ptp.getAmountBalance() != 0) {
				// TransferTxn Sender

				txnInsert.setTxnType("Send");
				txnInsert.setTxnState("Process");
				txnInsert.setTxnNote("test send");
				txnInsert.setFeeAmount(0.0);
				txnInsert.setSubmitAmount(ptp.getAmountBalance());
				txnInsert.setNetAmount(txnInsert.getFeeAmount() + txnInsert.getSubmitAmount());
				txnInsert.setSendBankCode("014");
				txnInsert.setSendAccountId(ptp.getSendAccountId());
				txnInsert.setReceiveBankCode(receiveFromPromptPay.getBankCode());
				txnInsert.setReceiveAccountId(receiveFromPromptPay.getAccountID());
				txnInsert.setAipid(Integer.toString(receiveFromPromptPay.getAIPID()));
				txnInsert.setCreateDtm(date);

				int ckInsert = transfer.updateTxn(txnInsert);
				int txnIdcmp = transfer.selectTxn(txnInsert);
				System.out.println(ckInsert);
				// Update balanceAmount
				if (ckInsert == 1) {
					int resSend = account.updateSendPromptPay(ptp);
					if (resSend == 1) {
						System.out.println(resSend);
						txnInsert.setTxnState("Complete");
						txnInsert.setTxnId(txnIdcmp);
						transfer.updateState(txnInsert);
						promptPayInfo req = new promptPayInfo();
						req.setAipid(receiveFromPromptPay.getAIPID());
						req.setSendBankCode(ptp.getSendBankCode());
						req.setSendAccountId(ptp.getSendAccountId());
						req.setAmountBalance(ptp.getAmountBalance());
						callRestService.transferPromptPay(req);
					} else {
						txnInsert.setTxnState("Fail");
						txnInsert.setTxnId(txnIdcmp);
						txnInsert.setNetAmount(0.0);
						transfer.updateState(txnInsert);
						throw new SQLException();
					}
				} else {
				}
			}
			ResultPromptPayInfo res = new ResultPromptPayInfo();
			res.setResult(txnInsert.getTxnState() + " for update balance amount.");
			return new ResponseEntity<ResultPromptPayInfo>(res, HttpStatus.OK);
		} catch (SQLException e) {
			throw new ExceptionDatabaseDown("Database fail.");
		} catch (ExceptionBalanceAmount e) {
			throw new ExceptionBalanceAmount("Balance amount incorrect!");
		}
	}
}
