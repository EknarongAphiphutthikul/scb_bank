package com.orcsoft.training.tulaya.connectDB;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.orcsoft.training.tulaya.model.Statement;
import com.orcsoft.training.tulaya.model.TransferStatement;
import com.orcsoft.training.tulaya.model.TransferTxnInfo;

public interface TransferTxnMapper {

	@Insert("Insert into TransferTxn (txnType, txnState, txnNote, feeAmount, submitAmount, netAmount,"
			+ "sendBankCode, sendAccountId, receiveBankCode, receiveAccountId, aipid, createDtm) "
			+ "values (#{txnType}, #{txnState}, #{txnNote}, #{feeAmount} "
			+ ", #{submitAmount}, #{netAmount}, #{sendBankCode} "
			+ ", #{sendAccountId}, #{receiveBankCode}, #{receiveAccountId} "
			+ ", #{aipid}, #{createDtm})")
	public int updateTxn(TransferTxnInfo req);
	
	@Select("Select txnId from TransferTxn where txnType=#{txnType} "
			+ "and netAmount=#{netAmount} and sendAccountId=#{sendAccountId} and receiveAccountId=#{receiveAccountId}"
			+ "and createDtm=#{createDtm}")
	public int selectTxn(TransferTxnInfo req);
	
	@Update("Update TransferTxn "
			+ "set txnState=#{txnState}, netAmount=#{netAmount} "
			+ "where txnId=#{txnId}")
	public int updateState(TransferTxnInfo req);
	
	@Select("select * from TransferTxn where (createDtm between #{dateStart} and #{dateEnd}) "
			+ "and (sendAccountId = #{accountId} "
			+ "or receiveAccountId = #{accountId}) "
			+ "and txnState='Complete' Order by createDtm desc")
	public List<TransferStatement> staementDuration(Statement req);
	
	@Select("select * from TransferTxn where (sendAccountId = #{accountId} AND txnType='Send') "
			+ "or (receiveAccountId = #{accountId} AND txnType='Receive') "
			+ "and txnState='Complete' Order by createDtm desc limit 5")
	public List<TransferStatement> staementFive(Statement req);
}
