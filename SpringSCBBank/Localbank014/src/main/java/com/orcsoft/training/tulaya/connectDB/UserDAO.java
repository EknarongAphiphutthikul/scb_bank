package com.orcsoft.training.tulaya.connectDB;

import org.apache.ibatis.session.SqlSession;

import com.orcsoft.training.tulaya.model.UserInfo;

public class UserDAO {

	public UserInfo authen(UserInfo req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		UserMapper usersMapper = session.getMapper(UserMapper.class);
		UserInfo users = usersMapper.authen(req);
		session.close();
		return users;
	}
}
