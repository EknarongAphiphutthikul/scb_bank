package com.orcsoft.training.tulaya.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TransferStatement {
	private String txnType;
	private double netAmount;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private Date createDtm;

	public Date getCreateDtm() {
		return createDtm;
	}
	
	public void setCreateDate(Date createDtm) {
		this.createDtm = createDtm;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

}
