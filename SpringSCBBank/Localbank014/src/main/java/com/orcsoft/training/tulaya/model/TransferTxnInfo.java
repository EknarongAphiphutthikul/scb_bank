package com.orcsoft.training.tulaya.model;

import java.util.Date;

public class TransferTxnInfo {

	private String aipid;
	private String txnType;
	private String txnState;
	private String txnNote;
	private double feeAmount;
	private double submitAmount;
	private double netAmount;
	private String sendBankCode;
	private String sendAccountId;
	private String receiveBankCode;
	private String receiveAccountId;
	private Date createDtm;
	private int txnId;
	
	public String getAipid() {
		return aipid;
	}

	public void setAipid(String aipid) {
		this.aipid = aipid;
	}

	public int getTxnId() {
		return txnId;
	}

	public void setTxnId(int txnId) {
		this.txnId = txnId;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public double getSubmitAmount() {
		return submitAmount;
	}

	public void setSubmitAmount(double submitAmount) {
		this.submitAmount = submitAmount;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

	public String getSendBankCode() {
		return sendBankCode;
	}

	public void setSendBankCode(String sendBankCode) {
		this.sendBankCode = sendBankCode;
	}

	public String getSendAccountId() {
		return sendAccountId;
	}

	public void setSendAccountId(String sendAccountId) {
		this.sendAccountId = sendAccountId;
	}

	public String getReceiveBankCode() {
		return receiveBankCode;
	}

	public void setReceiveBankCode(String receiveBankCode) {
		this.receiveBankCode = receiveBankCode;
	}

	public String getReceiveAccountId() {
		return receiveAccountId;
	}

	public void setReceiveAccountId(String receiveAccountId) {
		this.receiveAccountId = receiveAccountId;
	}

	public Date getCreateDtm() {
		return createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	public String getTxnState() {
		return txnState;
	}

	public void setTxnState(String txnState) {
		this.txnState = txnState;
	}

	public String getTxnNote() {
		return txnNote;
	}

	public void setTxnNote(String txnNote) {
		this.txnNote = txnNote;
	}
}
