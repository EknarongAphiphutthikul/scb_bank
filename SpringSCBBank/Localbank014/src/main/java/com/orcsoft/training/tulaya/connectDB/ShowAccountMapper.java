package com.orcsoft.training.tulaya.connectDB;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.orcsoft.training.tulaya.model.AccountInfo;
import com.orcsoft.training.tulaya.model.ReceivePromptPayInfo;
import com.orcsoft.training.tulaya.model.ShowAccountInfo;

public interface ShowAccountMapper {
	
	@Select("SELECT  CUSTOMER.CUSTOMERID, NAME ,ACCOUNTID ,BALANCEAMOUNT, BANKCODE, STATUSCODE FROM CUSTOMER"
			+ " Join Account on CUSTOMER.CUSTOMERID = ACCOUNT.CUSTOMERID"
			+ " WHERE CUSTOMER.CUSTOMERID=#{customerID} and STATUSCODE='ACT'")
	public List<ShowAccountInfo> getAccount(int customerID);
	
	@Select("SELECT NAME ,ACCOUNTID , BANKCODE, STATUSCODE FROM CUSTOMER"
			+ " Join Account on CUSTOMER.CUSTOMERID = ACCOUNT.CUSTOMERID"
			+ " WHERE ACCOUNTID=#{accountIdReceive} and STATUSCODE='ACT'")
	public List<ShowAccountInfo> getAccountReceive(AccountInfo act);
	
	@Select("SELECT ACCOUNTID , BANKCODE FROM CUSTOMER"
			+ " Join Account on CUSTOMER.CUSTOMERID = ACCOUNT.CUSTOMERID"
			+ " WHERE ACCOUNTID=#{receiveAccountId} and BankCode=#{receiveBankCode} ")
	public List<ShowAccountInfo> checkAccoucnt(ReceivePromptPayInfo act);
}
