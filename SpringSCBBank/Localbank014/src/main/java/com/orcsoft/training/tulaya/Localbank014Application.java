package com.orcsoft.training.tulaya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Localbank014Application {

	public static void main(String[] args) {
		SpringApplication.run(Localbank014Application.class, args);
	}
}
