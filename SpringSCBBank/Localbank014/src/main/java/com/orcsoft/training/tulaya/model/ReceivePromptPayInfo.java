package com.orcsoft.training.tulaya.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReceivePromptPayInfo {

	private String sendBankCode;
	private String sendAccountId;
	private String receiveBankCode;
	private String receiveAccountId;
	private double amount;

	public String getSendBankCode() {
		return sendBankCode;
	}

	@JsonProperty("SendBankCode")
	public void setSendBankCode(String sendBankCode) {
		this.sendBankCode = sendBankCode;
	}

	public String getSendAccountId() {
		return sendAccountId;
	}

	@JsonProperty("SendAccountID")
	public void setSendAccountId(String sendAccountId) {
		this.sendAccountId = sendAccountId;
	}

	public String getReceiveBankCode() {
		return receiveBankCode;
	}

	@JsonProperty("ReceiveBankCode")
	public void setReceiveBankCode(String receiveBankCode) {
		this.receiveBankCode = receiveBankCode;
	}

	public String getReceiveAccountId() {
		return receiveAccountId;
	}

	@JsonProperty("ReceiveAccountID")
	public void setReceiveAccountId(String receiveAccountId) {
		this.receiveAccountId = receiveAccountId;
	}

	public double getAmount() {
		return amount;
	}

	@JsonProperty("Amount")
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
