package com.orcsoft.training.tulaya.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Data;

@ControllerAdvice
public class ExceptionHandle {
	
	@ExceptionHandler(value = {ExceptionNotFound.class})
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	ResponseBodyException handleNotFoundException(ExceptionNotFound e) throws Exception{
		ResponseBodyException ex = new ResponseBodyException();
		ex.setErr(e.getMessage());
		return ex;
	}
	
	@ExceptionHandler(value = {ExceptionInternalError.class})
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	ResponseBodyException handleNotFoundException(ExceptionInternalError e) throws Exception{
		ResponseBodyException ex = new ResponseBodyException();
		ex.setErr(e.getMessage());
		return ex;
	}
	
	@ExceptionHandler(value = {ExceptionDatabaseDown.class})
	@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
	@ResponseBody
	ResponseBodyException handleNotFoundException(ExceptionDatabaseDown e) throws Exception{
		ResponseBodyException ex = new ResponseBodyException();
		ex.setErr(e.getMessage());
		return ex;
	}
	
	@ExceptionHandler(value = {ExceptionBalanceAmount.class})
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	ResponseBodyException handleNotFoundException(ExceptionBalanceAmount e) throws Exception{
		ResponseBodyException ex = new ResponseBodyException();
		ex.setErr(e.getMessage());
		return ex;
	}
	
	@Data
	class ResponseBodyException {
		private String err;
	}
}
