package com.orcsoft.training.tulaya.connectDB;

import org.apache.ibatis.annotations.Select;

import com.orcsoft.training.tulaya.model.UserInfo;

public interface UserMapper {

	@Select("SELECT customerID FROM CUSTOMER WHERE Login=#{login} And Password=#{password}")
	public UserInfo authen(UserInfo req);
}
