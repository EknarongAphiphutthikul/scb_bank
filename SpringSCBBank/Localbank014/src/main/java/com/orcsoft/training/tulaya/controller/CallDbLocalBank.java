package com.orcsoft.training.tulaya.controller;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orcsoft.training.tulaya.connectDB.AccountDAO;
import com.orcsoft.training.tulaya.connectDB.ShowAccountDAO;
import com.orcsoft.training.tulaya.connectDB.TransferTxnDAO;
import com.orcsoft.training.tulaya.connectDB.UserDAO;
import com.orcsoft.training.tulaya.model.AccountInfo;
import com.orcsoft.training.tulaya.model.ShowAccountInfo;
import com.orcsoft.training.tulaya.model.Statement;
import com.orcsoft.training.tulaya.model.TransferStatement;
import com.orcsoft.training.tulaya.model.TransferTxnInfo;
import com.orcsoft.training.tulaya.model.UpdateBalanceInfo;
import com.orcsoft.training.tulaya.model.UserInfo;

import lombok.Data;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/scb_bank")
public class CallDbLocalBank {

	@PostMapping(value = "/checkLogin", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseLogin checkUser(HttpServletRequest request, @RequestBody UserInfo req)
			throws ExceptionNotFound, ExceptionInternalError {
		try {
			UserDAO user = new UserDAO();
			UserInfo check = user.authen(req);

			ResponseLogin res = new ResponseLogin();
			res.setId(check.getCustomerID());
			return res;
		} catch (NullPointerException e) {
			throw new ExceptionNotFound("Incorrect username or password!");
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal server error.");
		}
	}

	@GetMapping(value = "/showAccount/{customerID}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<ShowAccountInfo> showAccount(HttpServletRequest request, @PathVariable("customerID") int customerID)
			throws ExceptionNotFound {
		ShowAccountDAO account = new ShowAccountDAO();
		List<ShowAccountInfo> act = account.getAccount(customerID);
		if (act.size() != 0) {
			return act;
		} else {
			throw new ExceptionNotFound("Account not found!");
		}
	}

	@PostMapping(value = "/getReceiveAccount", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<ShowAccountInfo> getReceivceAccount(HttpServletRequest request, @RequestBody AccountInfo req)
			throws ExceptionNotFound {
		ShowAccountDAO account = new ShowAccountDAO();
		List<ShowAccountInfo> act = account.getAccountReceive(req);
		if (act.size() != 0) {
			return act;
		} else {
			throw new ExceptionNotFound("Account not found!");
		}
	}

	@PostMapping(value = "/fiveStatement", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<TransferStatement> getFiveStatement(HttpServletRequest request, @RequestBody Statement stm)
			throws ExceptionInternalError {
		try {
			TransferTxnDAO txn = new TransferTxnDAO();
			List<TransferStatement> statement = txn.staementFive(stm);
			return statement;
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal server error.");
		}
	}

	@PostMapping(value = "/staementDuration", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<TransferStatement> staementDuration(HttpServletRequest request, @RequestBody Statement stm)
			throws ExceptionInternalError {
		try {
			TransferTxnDAO txn = new TransferTxnDAO();
			List<TransferStatement> statement = txn.staementDuration(stm);
			return statement;
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal server error.");
		}
	}

	@PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseTransfer transferUpdate(HttpServletRequest request, @RequestBody UpdateBalanceInfo act)
			throws ExceptionInternalError, ExceptionDatabaseDown, ExceptionBalanceAmount {
		try {
			AccountDAO account = new AccountDAO();
			TransferTxnInfo req = new TransferTxnInfo();
			TransferTxnDAO transfer = new TransferTxnDAO();
			Date date = new Date();

			if (account.getBalanceAmount(act) < act.getBalanceAmount() || act.getBalanceAmount() == 0) {
				throw new ExceptionBalanceAmount("Balance amount incorrect!");
			} else if (account.getBalanceAmount(act) >= act.getBalanceAmount() && act.getBalanceAmount() != 0) {
				// TransferTxn Sender
				req.setAipid(null);
				req.setTxnType("Send");
				req.setTxnState("Process");
				req.setTxnNote("test send");
				req.setFeeAmount(0.0);
				req.setSubmitAmount(act.getBalanceAmount());
				req.setNetAmount(req.getFeeAmount() + req.getSubmitAmount());
				req.setSendBankCode("014");
				req.setSendAccountId(act.getAccountIdSend());
				req.setReceiveBankCode("014");
				req.setReceiveAccountId(act.getAccountIdReceive());
				req.setCreateDtm(date);
				int insertSendTxn = transfer.updateTxn(req);
				int txnIdcmp = transfer.selectTxn(req);

				// TransferTxn Receiver
				req.setTxnType("Receive");
				req.setTxnState("Process");
				req.setTxnNote(null);
				req.setFeeAmount(0.0);
				int insertReceiveTxn = transfer.updateTxn(req);
				int txnId = transfer.selectTxn(req);

				// Update balanceAmount

				if (insertSendTxn == 1 && insertReceiveTxn == 1) {
					int resSend = account.updateSend(act);
					int restReceive = account.updateReceive(act);
					
					if (resSend == 1 && restReceive == 1) {
						req.setTxnState("Complete");
						req.setTxnId(txnIdcmp);
						transfer.updateState(req);
						req.setTxnId(txnId);
						transfer.updateState(req);
					} else {
						req.setTxnState("Fail");
						req.setTxnId(txnIdcmp);
						req.setNetAmount(0.0);
						transfer.updateState(req);
						req.setTxnId(txnId);
						transfer.updateState(req);
						throw new SQLException();
					} 
				} else {
					throw new SQLException();
				}
			}
			ResponseTransfer res = new ResponseTransfer();
			res.setMgs(req.getTxnState() + " for update balance amount.");
			return res;
		} catch (SQLException e) {
			throw new ExceptionDatabaseDown("Database fail.");
		} catch (ExceptionBalanceAmount e) {
			throw new ExceptionBalanceAmount("Balance amount incorrect!");
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal server error.");
		}
	}

	@Data
	class ResponseTransfer {
		private String mgs;
	}

	@Data
	class ResponseLogin {
		private int id;
	}

}