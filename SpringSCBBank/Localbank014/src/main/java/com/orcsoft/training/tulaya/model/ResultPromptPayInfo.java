package com.orcsoft.training.tulaya.model;

public class ResultPromptPayInfo {
	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
