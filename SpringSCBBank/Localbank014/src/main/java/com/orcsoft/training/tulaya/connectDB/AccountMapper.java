package com.orcsoft.training.tulaya.connectDB;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.orcsoft.training.tulaya.model.AccountInfo;
import com.orcsoft.training.tulaya.model.ReceivePromptPayInfo;
import com.orcsoft.training.tulaya.model.UpdateBalanceInfo;
import com.orcsoft.training.tulaya.model.UserPromptPayInfo;

public interface AccountMapper {

	@Update("UPDATE Account SET balanceAmount = balanceAmount - #{balanceAmount} "
			+ "where accountId = #{accountIdSend}")
	public int updateAccountSend(UpdateBalanceInfo act);
	
	@Update("UPDATE Account SET balanceAmount = balanceAmount - #{amountBalance} "
			+ "where accountId = #{sendAccountId}")
	public int updateBanlanceSend(UserPromptPayInfo act);
	
	@Update("UPDATE Account SET balanceAmount = balanceAmount + #{balanceAmount} "
			+ "where accountId = #{accountIdReceive}")
	public int updateAccountReceive(UpdateBalanceInfo act);
	
	@Update("UPDATE Account SET balanceAmount = balanceAmount - #{amountBalance} "
			+ "where accountId = #{sendAccountId}")
	public int updateSendPromptPay(UserPromptPayInfo act);
	
	@Update("UPDATE Account SET balanceAmount = balanceAmount + #{amount} "
			+ "where accountId = #{receiveAccountId}")
	public int updateReceivePromptPay(ReceivePromptPayInfo req);
	
	@Select("SELECT customerId FROM Account "
			+ "where accountId=#{accountIdReceive}")
	public AccountInfo getAcountId(AccountInfo req);
	
	@Select("SELECT balanceAmount FROM Account "
			+ "where accountId=#{accountIdSend}")
	public double getBalanceAmount(UpdateBalanceInfo req);
	
	@Select("SELECT balanceAmount FROM Account "
			+ "where accountId=#{sendAccountId}")
	public double getBalanceAmountPP(UserPromptPayInfo req);
}
