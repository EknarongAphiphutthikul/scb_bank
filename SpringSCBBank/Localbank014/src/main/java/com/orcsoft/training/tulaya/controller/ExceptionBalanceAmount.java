package com.orcsoft.training.tulaya.controller;

@SuppressWarnings("serial")
public class ExceptionBalanceAmount extends Exception {
	public ExceptionBalanceAmount(String msg) {
		super(msg);
	}
}