package com.orcsoft.training.tulaya.model;

public class UserPromptPayInfo {
	private String sendAccountId;
	private String sendBankCode;
	private String type;
	private String value;
	private double amountBalance;

	public String getSendAccountId() {
		return sendAccountId;
	}

	public void setSendAccountId(String sendAccountId) {
		this.sendAccountId = sendAccountId;
	}

	public String getSendBankCode() {
		return sendBankCode;
	}

	public void setSendBankCode(String sendBankCode) {
		this.sendBankCode = sendBankCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public double getAmountBalance() {
		return amountBalance;
	}

	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
	}

}
