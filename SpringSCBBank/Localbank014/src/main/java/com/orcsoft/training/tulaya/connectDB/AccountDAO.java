package com.orcsoft.training.tulaya.connectDB;

import org.apache.ibatis.session.SqlSession;

import com.orcsoft.training.tulaya.model.AccountInfo;
import com.orcsoft.training.tulaya.model.ReceivePromptPayInfo;
import com.orcsoft.training.tulaya.model.UpdateBalanceInfo;
import com.orcsoft.training.tulaya.model.UserPromptPayInfo;

public class AccountDAO {

	public AccountInfo getAccount(AccountInfo req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		AccountInfo act = actMapper.getAcountId(req);
		session.close();
		return act;
	}

	public int updateSend(UpdateBalanceInfo act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		int res = actMapper.updateAccountSend(act);
		if (res != 1) {
			session.rollback();
		} else {
			session.commit();
		}
		session.close();
		return res;
	}

	public int updateSendPromptPay(UserPromptPayInfo act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		int res = actMapper.updateSendPromptPay(act);
		if (res != 1) {
			session.rollback();
		} else {
			session.commit();
		}
		session.close();
		return res;
	}
	
	public int updateReceivePromptPay(ReceivePromptPayInfo req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		int res = actMapper.updateReceivePromptPay(req);
		if (res != 1) {
			session.rollback();
		} else {
			session.commit();
		}
		session.close();
		return res;
	}

	public int updateBanlanceSend(UserPromptPayInfo act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		int res = actMapper.updateBanlanceSend(act);
		session.commit();
		session.close();
		return res;
	}

	public int updateReceive(UpdateBalanceInfo act) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		int res = actMapper.updateAccountReceive(act);
		if (res != 1) {
			session.rollback();
		} else {
			session.commit();
		}
		session.close();
		return res;
	}

	public double getBalanceAmount(UpdateBalanceInfo req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		double act = actMapper.getBalanceAmount(req);
		session.close();
		return act;
	}

	public double getBalanceAmountPP(UserPromptPayInfo req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper actMapper = session.getMapper(AccountMapper.class);
		double act = actMapper.getBalanceAmountPP(req);
		session.close();
		return act;
	}
}
