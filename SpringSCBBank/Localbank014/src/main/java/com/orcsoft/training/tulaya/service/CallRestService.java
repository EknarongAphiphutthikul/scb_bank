package com.orcsoft.training.tulaya.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.orcsoft.training.tulaya.controller.ExceptionInternalError;
import com.orcsoft.training.tulaya.controller.ExceptionNotFound;
import com.orcsoft.training.tulaya.model.GetPromptPayInfo;
import com.orcsoft.training.tulaya.model.RegisterInfo;
import com.orcsoft.training.tulaya.model.promptPayInfo;

import lombok.Data;

@Service
public class CallRestService {

	String uri = "http://192.168.9.154:8090/interbank/";
	private RestTemplate restTemplate = new RestTemplate();

	public ResponseEntity<GetStatusCode> registerInfo(RegisterInfo req) throws ExceptionInternalError {
		try {
			restTemplate.postForObject(uri + "any-id", req, String.class);
			GetStatusCode res = new GetStatusCode();
			res.setStatus("Registration successfully.");
			return new ResponseEntity<GetStatusCode>(res, HttpStatus.OK);
		} catch (Exception e) {
			throw new ExceptionInternalError(e.getMessage().toString());
		}
	}

	public GetPromptPayInfo getPromptPay(String type, String value) throws ExceptionNotFound {
		try {
			GetPromptPayInfo respGetP = restTemplate.getForObject(uri + "any-id/?type=" + type + "&value=" + value,
					GetPromptPayInfo.class);
			return respGetP;
		} catch (HttpClientErrorException e) {
			throw new ExceptionNotFound("Account not found.");
		}
	}

	public ResponseEntity<GetStatusCode> deletePromptPay(int apid) throws ExceptionNotFound {
		try {
			restTemplate.delete(uri + "any-id/" + apid);
			GetStatusCode res = new GetStatusCode();
			res.setStatus("Delete account success.");
			return new ResponseEntity<GetStatusCode>(res, HttpStatus.OK);
		} catch (HttpClientErrorException e) {
			throw new ExceptionNotFound("Account not found.");
		}
	}

	public promptPayInfo transferPromptPay(promptPayInfo req) {
		HttpHeaders header = new HttpHeaders();
		header.add(HttpHeaders.CONTENT_TYPE, "application/json");
		HttpEntity<?> postReq = new HttpEntity<Object>(req, header);
		ResponseEntity<promptPayInfo> out = restTemplate.exchange(uri + "/money-tranfer", HttpMethod.POST, postReq,
				promptPayInfo.class);
//		String res = restTemplate.postForObject(uri + "money-tranfer", req, String.class);
		return out.getBody();
	}

	@Data
	public class GetStatusCode {
		private String status;
	}
}
