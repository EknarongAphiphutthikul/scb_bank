package com.orcsoft.training.tulaya.controller;

@SuppressWarnings("serial")
public class ExceptionNotFound extends Exception {
	public ExceptionNotFound(String msg) {
		super(msg);
	}
}
