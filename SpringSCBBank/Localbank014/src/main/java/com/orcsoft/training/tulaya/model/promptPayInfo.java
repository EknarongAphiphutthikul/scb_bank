package com.orcsoft.training.tulaya.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class promptPayInfo {

	private String sendAccountId;
	private String sendBankCode;
	private int aipid;
	private double amountBalance;
	
	public int getAipid() {
		return aipid;
	}

	@JsonProperty("AIPID")
	public void setAipid(int aipid) {
		this.aipid = aipid;
	}

	public String getSendAccountId() {
		return sendAccountId;
	}

	@JsonProperty("SendAccountID")
	public void setSendAccountId(String sendAccountId) {
		this.sendAccountId = sendAccountId;
	}

	public String getSendBankCode() {
		return sendBankCode;
	}

	@JsonProperty("SendBankCode")
	public void setSendBankCode(String sendBankCode) {
		this.sendBankCode = sendBankCode;
	}

	public double getAmountBalance() {
		return amountBalance;
	}

	@JsonProperty("Amount")
	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
	}

}
