package com.orcsoft.training.tulaya.controller;

@SuppressWarnings("serial")
public class ExceptionDatabaseDown extends Exception{
	public ExceptionDatabaseDown(String msg) {
		super(msg);
	}
}
