package com.orcsoft.training.tulaya.model;

public class AccountInfo {
	private int customerId;
	private String accountIdReceive;

	public String getAccountIdReceive() {
		return accountIdReceive;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public void setAccountIdReceive(String accountIdReceive) {
		this.accountIdReceive = accountIdReceive;
	}
}