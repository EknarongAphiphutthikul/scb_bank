package com.orcsoft.training.tulaya.controller;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.orcsoft.training.tulaya.connectDB.AccountDAO;
import com.orcsoft.training.tulaya.connectDB.ShowAccountDAO;
import com.orcsoft.training.tulaya.connectDB.TransferTxnDAO;
import com.orcsoft.training.tulaya.model.ReceivePromptPayInfo;
import com.orcsoft.training.tulaya.model.ShowAccountInfo;
import com.orcsoft.training.tulaya.model.TransferTxnInfo;

import lombok.Data;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/scbbankReceive")
public class CallRestApiReceive {

	@PostMapping(value = "/receivePromptPay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResultReceivePromptPpay receivePromptPay(HttpServletRequest request, @RequestBody ReceivePromptPayInfo req)
			throws ExceptionNotFound, ExceptionDatabaseDown, ExceptionInternalError {

		TransferTxnInfo txnInsert = new TransferTxnInfo();
		TransferTxnDAO transfer = new TransferTxnDAO();
		AccountDAO account = new AccountDAO();
		ShowAccountDAO act = new ShowAccountDAO();
		Date date = new Date();

		try {
			List<ShowAccountInfo> ck = act.checkAccoucnt(req);
			
			txnInsert.setTxnType("Receive");
			txnInsert.setTxnState("Process");
			txnInsert.setTxnNote(null);
			txnInsert.setFeeAmount(0.0);
			txnInsert.setSubmitAmount(req.getAmount());
			txnInsert.setNetAmount(txnInsert.getFeeAmount() + txnInsert.getSubmitAmount());
			txnInsert.setSendBankCode(req.getSendBankCode());
			txnInsert.setSendAccountId(req.getSendAccountId());
			txnInsert.setReceiveBankCode(ck.get(0).getBankCode());
			txnInsert.setReceiveAccountId(ck.get(0).getAccountId());
			txnInsert.setAipid(null);
			txnInsert.setCreateDtm(date);

			int ckUpdate = transfer.updateTxn(txnInsert);
			int txnIdcmp = transfer.selectTxn(txnInsert);

			if (ckUpdate == 1) {
				int resReceive = account.updateReceivePromptPay(req);
				if (resReceive == 1) {
					txnInsert.setTxnState("Complete");
					txnInsert.setTxnId(txnIdcmp);
					transfer.updateState(txnInsert);
					
					ResultReceivePromptPpay res = new ResultReceivePromptPpay();
					res.setTxnRefId(txnInsert.getTxnId());
					res.setTxnDtm(txnInsert.getCreateDtm());
					res.setAmount(txnInsert.getSubmitAmount());
					res.setResult("Success.");
					return res;
				} else {
					txnInsert.setTxnState("Fail");
					txnInsert.setTxnId(txnIdcmp);
					transfer.updateState(txnInsert);
					
					ResultReceivePromptPpay res = new ResultReceivePromptPpay();
					res.setTxnRefId(txnInsert.getTxnId());
					res.setTxnDtm(txnInsert.getCreateDtm());
					res.setAmount(txnInsert.getSubmitAmount());
					res.setResult("Fail.");
					return res;
				}
			} else {
				throw new SQLException();
			}
		} catch (IndexOutOfBoundsException e) {
			throw new ExceptionNotFound("Account not found.");
		} catch (SQLException e) {
			throw new ExceptionDatabaseDown("Database fail.");
		} 
		catch (Exception e) {
			throw new ExceptionInternalError("Internal server error.");
		}
	}

	@Data
	class ResultReceivePromptPpay {
		@JsonProperty("TxnRefID")
		private int txnRefId;
		@JsonProperty("TxnDTM")
		@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		private Date txnDtm;
		@JsonProperty("Amount")
		private double amount;
		@JsonProperty("Result")
		private String result;
	}
}
